package de.oszimt.starsim2099;

/**
 * Write a description of class Pilot here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Pilot {

	// Attribute
	private double posx;
	private double posy;
	private String grad;
	private String name;
	// Methoden
	//getter
	public double getPosX() { return posx; }
	public double getPosY() { return posy; }
	public String getGrad() { return grad; }
    public String getName() { return name; }
    //setter
   public void setPosX (double posx) { this.posx = posx; }
   public void setPosY (double posy) { this.posy = posy; }
   public void setGrad (String grad) { this.grad = grad; }
   public void setName (String name) { this.name = name; }
}
