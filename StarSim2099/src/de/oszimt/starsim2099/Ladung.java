package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Ladung {

	// Attribute
	private double PosX;
	private double PosY;
	private int masse;
	private String typ;
	// Methoden
	//getter
	public double getPosX () { return PosX;  }
	public double getPosY () { return PosY;  }
	public int getMasse   () { return masse; }
	public String getTyp  () { return typ;   }
	//setter
	public void setPosX (double PosX) { this.PosX = PosX;}
	public void setPosY (double PosY) { this.PosY = PosY;}
	public void setMasse (int Masse) { this.masse = Masse;}
	public void setTyp (String Typ) { this.typ = Typ;}
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}