package biner_linierLaufzeiten;

public class Binaer {

	/**
	 * Bin�re Suche auf dem Array M nach dem Eintrag suchwert
	 *
	 * @param M :          zu durchsuchendes Feld
	 * @param eintraege :  Anzahl der Feldelemente
	 * @param suchwert :   gesuchter Eintrag
	 * @return >= 0: Index des gesuchten Eintrags
	 *         <  0: nichts gefunden
	 */
	int binary_search(final int M[], final int eintraege, final int suchwert)
	{
	    final int NICHT_GEFUNDEN = -1 ;
	    int links = 0;                      /* man beginne beim kleinsten Index */
	    int rechts = eintraege - 1;         /* Arrays-Index beginnt bei 0 */

	    /* Solange die zu durchsuchende Menge nicht leer ist */
	    while (links <= rechts)
	    {
	        int mitte = links + ((rechts - links) / 2); /* Bereich halbieren */

	        if (M[mitte] == suchwert)       /* Element suchwert gefunden? */
	            return mitte;
	        else
	            if (M[mitte] > suchwert)
	                rechts = mitte - 1;     /* im linken Abschnitt weitersuchen */
	            else /* (M[mitte] < suchwert) */
	                links = mitte + 1;      /* im rechten Abschnitt weitersuchen */
	            /* end if */
	        /* end if */
	    }

	    return NICHT_GEFUNDEN;
	    // alternativ: return -links; // (-Returnwert) zeigt auf kleinstes Element >= suchwert: Platz f�r Einf�gen
	}

}
