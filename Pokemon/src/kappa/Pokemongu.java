package kappa;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.Icon;
import javax.swing.JTextField;
import javax.swing.JProgressBar;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JButton;


public class Pokemongu extends JPanel {
	private JTextField txtName;
	/**
	 * Create the panel.
	 */
	public Pokemongu() {
		setLayout(null);
		
		
		JLabel lblwp = new JLabel("WP");
		lblwp.setBounds(283, 11, 53, 20);
		lblwp.setVerticalAlignment(SwingConstants.TOP);
		lblwp.setFont(new Font("Tahoma", Font.PLAIN, 16));
		add(lblwp);
		
		JLabel lblBild = new JLabel(new ImageIcon("C:\\Users\\user\\Desktop\\ponit2a2.png"));
		lblBild.setFont(new Font("Tahoma", Font.PLAIN, 38));
		lblBild.setBounds(0, 0, 674, 319);
		lblBild.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblBild);
		
		JLabel lblKp = new JLabel("KP");
		lblKp.setHorizontalAlignment(SwingConstants.RIGHT);
		lblKp.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblKp.setBounds(213, 410, 64, 20);
		add(lblKp);
		
		txtName = new JTextField();
		txtName.setText("Name");
		txtName.setBounds(219, 325, 163, 50);
		add(txtName);
		txtName.setColumns(10);
		
		JLabel lblMaxKP = new JLabel("MAXKP");
		lblMaxKP.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblMaxKP.setBounds(298, 410, 125, 20);
		add(lblMaxKP);
		
		JLabel lblslash = new JLabel("/");
		lblslash.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblslash.setBounds(283, 410, 87, 20);
		add(lblslash);
		
		JProgressBar fortschirtt = new JProgressBar();
		fortschirtt.setBounds(213, 385, 194, 14);
		add(fortschirtt);
		
		JCheckBox chkBxFaveroite = new JCheckBox("");
		chkBxFaveroite.setBounds(609, 11, 75, 77);
		add(chkBxFaveroite);
		
		JLabel lbstern = new JLabel(new ImageIcon("SternLeer.png"));
		lbstern.setBounds(588, 39, 75, 49);
		add(lbstern);
		
		JLabel lblTyp1 = new JLabel("Typ1");
		lblTyp1.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 16));
		lblTyp1.setBounds(70, 444, 130, 43);
		add(lblTyp1);
		
		JLabel label = new JLabel("/");
		label.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 16));
		label.setBounds(104, 434, 80, 62);
		add(label);
		
		JLabel lblTyp = new JLabel("Typ2");
		lblTyp.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 16));
		lblTyp.setBounds(115, 440, 53, 50);
		add(lblTyp);
		
		JLabel lblTyp_1 = new JLabel("Typ");
		lblTyp_1.setBounds(94, 483, 46, 14);
		add(lblTyp_1);
		
		JLabel lblKg = new JLabel("kg");
		lblKg.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 16));
		lblKg.setBounds(298, 451, 46, 28);
		add(lblKg);
		
		JLabel lblGewicht = new JLabel("Gewicht");
		lblGewicht.setBounds(283, 482, 46, 14);
		add(lblGewicht);
		
		JLabel lblM = new JLabel("m");
		lblM.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 16));
		lblM.setBounds(485, 451, 46, 30);
		add(lblM);
		
		JLabel lblGewicht_1 = new JLabel("0");
		lblGewicht_1.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 16));
		lblGewicht_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblGewicht_1.setBounds(268, 455, 46, 20);
		add(lblGewicht_1);
		
		JLabel lblGroesse1 = new JLabel("0");
		lblGroesse1.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 16));
		lblGroesse1.setBounds(474, 451, 46, 30);
		add(lblGroesse1);
		
		JLabel lblGre = new JLabel("Gr\u00F6\u00DFe");
		lblGre.setBounds(474, 483, 46, 14);
		add(lblGre);
		
		JSeparator separator = new JSeparator();
		separator.setForeground(Color.BLACK);
		separator.setBounds(0, 508, 684, 2);
		add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setForeground(Color.BLACK);
		separator_1.setBounds(0, 521, 684, 2);
		add(separator_1);
		
		JLabel lblSternstaub = new JLabel("0");
		lblSternstaub.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 16));
		lblSternstaub.setBounds(104, 534, 46, 31);
		add(lblSternstaub);
		
		ImageIcon SSI= new ImageIcon("SSI.png");
		
		JLabel lblSternstaubicon = new JLabel(SSI);
		lblSternstaubicon.setBounds(52, 534, 46, 31);
		add(lblSternstaubicon);
		
		JLabel lblSternstaub_1 = new JLabel("STERNENSTAUB");
		lblSternstaub_1.setBounds(70, 565, 87, 20);
		add(lblSternstaub_1);
		
		JLabel lblBonBonAnzahl = new JLabel("0");
		lblBonBonAnzahl.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 16));
		lblBonBonAnzahl.setBounds(456, 542, 46, 14);
		add(lblBonBonAnzahl);
		
		ImageIcon BBI = new ImageIcon("bonbon.png");
		
		JLabel lblIconBonBon = new JLabel(new ImageIcon("C:\\Users\\user\\Desktop\\bonbon.png"));
		lblIconBonBon.setBounds(261, 563, 75, 57);
		add(lblIconBonBon);
		
		JLabel lblName2 = new JLabel("Name");
		lblName2.setBounds(410, 565, 46, 20);
		add(lblName2);
		
		JLabel lblbonbon = new JLabel("- BONBON");
		lblbonbon.setBounds(449, 565, 71, 20);
		add(lblbonbon);
		
		JButton btnLevln = new JButton("POWER-UP");
		btnLevln.setBounds(70, 608, 89, 23);
		add(btnLevln);
		
		JLabel lblSternstaub_2 = new JLabel("0");
		lblSternstaub_2.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 13));
		lblSternstaub_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblSternstaub_2.setBounds(219, 608, 58, 23);
		add(lblSternstaub_2);
		
		JLabel lblIconSS2 = new JLabel(SSI);
		lblIconSS2.setBounds(169, 608, 46, 23);
		add(lblIconSS2);
		
		JLabel lblbonbonicon2 = new JLabel(BBI);
		lblbonbonicon2.setBounds(431, 608, 46, 23);
		add(lblbonbonicon2);
		
		JLabel lblNewLabel = new JLabel("0");
		lblNewLabel.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 13));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(474, 611, 46, 20);
		add(lblNewLabel);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(35, 642, 613, 5);
		add(separator_2);
		
		JLabel lblAttacke = new JLabel("Attacke1");
		lblAttacke.setBounds(63, 658, 64, 28);
		add(lblAttacke);
		
		JLabel lblTypattacke1 = new JLabel("TypAttacke");
		lblTypattacke1.setBounds(62, 686, 88, 14);
		add(lblTypattacke1);
		
		JLabel lblSchaden = new JLabel("0");
		lblSchaden.setBounds(575, 665, 46, 14);
		add(lblSchaden);
		
		JLabel Attacke2 = new JLabel("Attacke2");
		Attacke2.setBounds(62, 711, 46, 28);
		add(Attacke2);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBounds(72, 750, 46, 14);
		add(lblNewLabel_1);
		
	}
	public static void main(String[] args) {
		
		
		JPanel pnl = new Pokemongu();
		JFrame frame = new JFrame();
		frame.setContentPane(pnl);
		frame.setBounds(100,100,500,500);
		frame.setVisible(true);
	}
}