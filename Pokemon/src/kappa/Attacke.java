package kappa;

public class Attacke {
	private String name;
	private String typ;
	private int schaden;
	//Konstruktor
	public Attacke(String name, String typ, int schaden) {
		this.name = name;
		this.typ = typ;
		this.schaden = schaden;
	}
	public Attacke() {
		this.name = name;
		this.typ = typ;
		this.schaden = schaden;
	}
	//getter und setter
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public int getSchaden() {
		return schaden;
	}
	public void setSchaden(int schaden) {
		this.schaden = schaden;
	}
	//methoden
	@Override
	public String toString() {
		return "Attacke [name=" + name + ", typ=" + typ + ", schaden=" + schaden + "]";
	}
}
