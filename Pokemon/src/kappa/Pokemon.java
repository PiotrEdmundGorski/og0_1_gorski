package kappa;

public class Pokemon {
	private String name;
	private int kp;
	private int wp;
	private String typ;
	private double gewicht;
	private double gr��e;
	private String fangdatum;
	private int sternstaub;
	private int bonbon;
	private Attacke attack;
	private int maxKp;
	private boolean favorite;
	
	public Pokemon(String name, int kp, String typ, double gewicht, double gr��e, int wp, String fangdatum, int sternstaub, int bonbon, Attacke attack, int maxKp, boolean favorite) {
		this.name = name;
		this.kp = kp;
		this.typ = typ;
		this.gewicht = gewicht;
		this.gr��e = gr��e;
		this.fangdatum = fangdatum;
		this.sternstaub = sternstaub;
		this.bonbon = bonbon;
		this.attack = attack;
		this.maxKp = maxKp;
		this.favorite = favorite;
	}
	public Pokemon() {
		this.name = name;
		this.kp = kp;
		this.typ = typ;
		this.gewicht = gewicht;
		this.gr��e = gr��e;
		this.fangdatum = fangdatum;
		this.sternstaub = sternstaub;
		this.bonbon = bonbon;
		this.attack = attack;
		this.maxKp = maxKp;
		this.favorite = favorite;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getKp() {
		return kp;
	}
	public void setKp(int kp) {
		this.kp = kp;
	}
	public int getWp() {
		return wp;
	}
	public void setWp(int wp) {
		this.wp = wp;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public double getGewicht() {
		return gewicht;
	}
	public void setGewicht(double gewicht) {
		this.gewicht = gewicht;
	}
	public double getGr��e() {
		return gr��e;
	}
	public void setGr��e(double gr��e) {
		this.gr��e = gr��e;
	}
	public String getFangdatum() {
		return fangdatum;
	}
	public void setFangdatum(String fangdatum) {
		this.fangdatum = fangdatum;
	}
	
	public int getSternstaub() {
		return sternstaub;
	}
	public void setSternstaub(int sternstaub) {
		this.sternstaub = sternstaub;
	}
	public int getBonbon() {
		return bonbon;
	}
	public void setBonbon(int bonbon) {
		this.bonbon = bonbon;
	}
	public Attacke getAttack() {
		return attack;
	}
	public void setAttack(Attacke attack) {
		this.attack = attack;
	}
	public int getMaxKp() {
		return maxKp;
	}
	public void setMaxKp(int maxKp) {
		this.maxKp = maxKp;
	}
	public boolean isFavorite() {
		return favorite;
	}
	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}
	//methoden
	public void angreifen() {
		
	}
	@Override
	public String toString() {
		return "Pokemon [name=" + name + ", kp=" + kp + ", wp=" + wp + ", typ=" + typ + ", gewicht=" + gewicht
				+ ", gr��e=" + gr��e + ", fangdatum=" + fangdatum + ", sternstaub=" + sternstaub + ", bonbon=" + bonbon
				+ ", attack=" + attack + ", maxKp=" + maxKp + ", favorite=" + favorite + "]";
	}
}
