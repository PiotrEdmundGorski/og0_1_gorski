package primzahl;

import java.util.Scanner;

public class test {

	public static void main(String[] args) {

		Primzahl p1 = new Primzahl();
		Scanner sc = new Scanner(System.in);
		System.out.println("Gib bitte eine Natürliche Zahl um zu erfahren ob die eine Primzahl ist! ");
		long x = sc.nextLong();
		long timestart = System.currentTimeMillis();
		System.out.println(p1.isPrimzahl(x));
		long timeend = System.currentTimeMillis();
		long time = timeend - timestart;
		System.out.println("Es Dauert: " + time + "ms");

	}

}
