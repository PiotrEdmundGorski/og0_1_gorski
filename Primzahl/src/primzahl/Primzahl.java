package primzahl;



public class Primzahl {

	public boolean isPrimzahl(long zahl) {
		long x;
		if (zahl <= 1) {
			return false;
		} else if (zahl == 2) {
			return true;
		} else {

			for (long i = zahl - 1; i > 1; i--) {

				x = zahl % i;
				if (x == 0) {
					i = 0;
					return false;
				} 

			}
			return true;
		}
	
	}
}