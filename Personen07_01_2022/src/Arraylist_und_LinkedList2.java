import java.util.LinkedList;

public class Arraylist_und_LinkedList2 {

	public static void main(String[] args) {
		LinkedList<Integer> simon = new LinkedList<Integer>();
		
		simon.add(0);
		simon.add(1);
		simon.add(2);
		simon.add(3);
		simon.add(4);
		simon.add(5);
		simon.add(14);
		System.out.println(simon);
	
		
		System.out.println("size: "+simon.size());
		System.out.println("remove 5: "+ simon.remove(5));
		System.out.println(simon);
		System.out.println("get 5: "+simon.get(5));
		simon.clear();
		System.out.println("clear: "+simon);
		
		
		
	}
	
	
}
