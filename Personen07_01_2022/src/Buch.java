
public class Buch implements Comparable<Buch>{
	
	private String autor;
	private String titel;
	private String personalId;
	
	public Buch (String autor,String titel,String personalId) {
		this.autor = autor;
		this.titel = titel;
		this.personalId = personalId;
	}
	
	public String getAutor() {
		return autor;
	}
	public String getTitel() {
		return titel;
	}
	public String getPersonalId() {
		return personalId;
	}
	public void setAutor (String autor) {
		this.autor = autor;
	}
	public void setTitel (String titel) {
		this.titel = titel;
	}
	public void setPersonalId (String personalId) {
		this.personalId = personalId;
	}
	
	public boolean equals(Buch b){
		return b.getPersonalId().equals(this.personalId);
	}
	
	
	public int compareTo(Buch b){
		return this.personalId.compareTo(b.getPersonalId());
	
	}
	

	
}
