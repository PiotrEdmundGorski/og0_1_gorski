package toyconzoo;

public class Addon {

	// Attribute
	private String bezeichnung;
	private String art;
	private int idNummer;
	private double verkaufspreiss;
	private int anz_addon;
	private int anz_addon_max;

	// Methoden
	public void verbrauch(int minusAnz) {
		anz_addon -= minusAnz;
	}

	public void kauf(int plusAnz) {
		anz_addon += plusAnz;
	}

	public void achievements() {

	}

	public double gesamtwertAddons() {
		return (anz_addon * verkaufspreiss);
	}

	// getter und setter
	public Addon() {
	}

	public String getbezeichnung() {
		return bezeichnung;
	}

	public void setbezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public String getart() {
		return art;
	}

	public void setart(String art) {
		this.art = art;
	}

	public int getidNummer() {
		return idNummer;
	}

	public void setidNummer(int idNummer) {
		this.idNummer = idNummer;
	}

	public double getverkaufspreiss() {
		return verkaufspreiss;
	}

	public void setverkaufspreiss(double verkaufspreiss) {
		this.verkaufspreiss = verkaufspreiss;
	}

	public int getanz_addon() {
		return anz_addon;
	}

	public void setanz_addon(int anz_addon) {
		this.anz_addon = anz_addon;
	}

	public int getanz_addon_max() {
		return anz_addon_max;
	}

	public void setanz_addon_max(int anz_addon_max) {
		this.anz_addon_max = anz_addon_max;
	}

}