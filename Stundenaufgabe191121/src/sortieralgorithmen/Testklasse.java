package sortieralgorithmen;

public class Testklasse {

	public static void printlist(int[] feld) {
		for (int i = 0; i < feld.length; i++) {
			
			System.out.print(feld[i]+", ");
		}
		System.out.println("");
	}

	public static void main(String[] args) {
		Selectsort2 s1 = new Selectsort2();

		int[] best = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };
		int[] aveyrage = { 0, 2, 3, 7, 10, 19, 5, 18, 15, 1, 6, 9, 8, 4, 12, 11, 13, 16, 17, 14 };
		int[] worst = { 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
		
		System.out.println("Best case");
		System.out.print("Bevor sortierung:  ");
		printlist (best);
		System.out.print("Nach dem Sortiert: ");
		printlist (s1.sort(best));
		System.out.println("Vertausche: "+ s1.vertausch);
		System.out.println("Vergleiche: "+ s1.vergleiche);
		System.out.println("");
		
		System.out.println("Average case:");
		System.out.print("Bevor sortierung:  ");
		printlist (aveyrage);
		System.out.print("Nach dem Sortiert: ");
		printlist (s1.sort(aveyrage));
		System.out.println("Vertausche: "+ s1.vertausch);
		System.out.println("Vergleiche: "+ s1.vergleiche);
		System.out.println("");
		
		System.out.println("Worst case:");
		System.out.print("Bevor sortierung:  ");
		printlist (worst);
		System.out.print("Nach dem Sortiert: ");
		printlist (s1.sort(worst));
		System.out.println("Vertausche: "+ s1.vertausch);
		System.out.println("Vergleiche: "+ s1.vergleiche);
	}
}
