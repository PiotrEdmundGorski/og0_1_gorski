package sortieralgorithmen;

public class Selectsort2 {
	public int vertausch = 0;
	public int vergleiche = 0;
	public int[] sort(int[] elements) {
		 vertausch = 0;
		 vergleiche = 0;
		int length = elements.length;
		for (int i = 0; i < length - 1; i++) {
			// Search the smallest element in the remaining array
			int minPos = i;
			int min = elements[minPos];
			for (int j = i + 1; j < length; j++) {
				vergleiche++;
				if (elements[j] < min) {
					vertausch++;
					minPos = j;
					min = elements[minPos];
				}
			}

			// Swap min with element at pos i
			if (minPos != i) {
				elements[minPos] = elements[i];
				elements[i] = min;
			}
		}
		return elements;
		
	}
}