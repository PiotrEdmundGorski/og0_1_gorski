package kaystore;

public class KeyStore01 {
private String[] keys; 
private int currentPos;
public static final int MAXPOSITION = 100;

	public KeyStore01() {
		this.keys = new String [MAXPOSITION];
		this.currentPos = 0;
	}	
	public KeyStore01(int lenght) {
		this.keys = new String [lenght];
		this.currentPos = 0;
	}
	
		
	
	public boolean add(String schlussel) {
/*		for (int i = 0; i < keys.length; i++) {
			if (keys[i] == 0) {
				keys[i]=schlussel;
				return true;
			}
		}*/
		if (currentPos < MAXPOSITION) {
			this.keys[this.currentPos] = schlussel;
			this.currentPos++;
			return true;
		}
		return false;
	}

	public int indexOf(String eintrag) {
		for (int i = 0; i < this.currentPos; i++) {
			if (this.keys[i].equals(eintrag)) {
				return i;
			}
		}
		return -1;
	}

	public void remove(int index) {
		if(index >= 0 && index < currentPos) {
			for (int i = index; i < currentPos; i++) {
				this.keys[i] = this.keys[i+1];
			}
			currentPos--;
		}
		
	}
	
	
}
