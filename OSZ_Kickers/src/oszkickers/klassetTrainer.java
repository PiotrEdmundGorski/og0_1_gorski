package oszkickers;

public class klassetTrainer extends KlasePerson {
//Atribute
	
	private char lizenzklasse;
	private double aufwandentschaedigung;
	
	//getter 

	public klassetTrainer(String new_name, int new_telefon_nr, boolean new_bezahlen, char new_lizenzklasse, double new_aufwandentschaedigung) {
		super(new_name, new_telefon_nr, new_bezahlen);
		
		lizenzklasse = new_lizenzklasse;
		aufwandentschaedigung = new_aufwandentschaedigung;
	
	}
	public char getLizenzklasse() {return lizenzklasse;}
	public double getAufwandentschaedigung() {return aufwandentschaedigung;}
	//setter
	
	public void setLizenzklasse (char lizenzklasse) {this.lizenzklasse = lizenzklasse;}
	public void setAufwandentschaedigung (double aufwandentschaedigung) {this.aufwandentschaedigung = aufwandentschaedigung;}
}
