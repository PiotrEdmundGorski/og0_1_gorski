package oszkickers;

public class klasseSpieler extends KlasePerson {
//atribute
private int trikot_nr;
private int spielpostion;


public klasseSpieler(String new_name, int new_telefon_nr, boolean new_bezahlen, int new_trikot_nr, int new_spielpostion) {
	super(new_name, new_telefon_nr, new_bezahlen);
	
	trikot_nr = new_trikot_nr;
	spielpostion = new_spielpostion;
}
//getter und setter


public int getTrikot_nr() {
	return trikot_nr;
}


public void setTrikot_nr(int trikot_nr) {
	this.trikot_nr = trikot_nr;
}


public int getSpielpostion() {
	return spielpostion;
}


public void setSpielpostion(int spielpostion) {
	this.spielpostion = spielpostion;
}


}


