package oszkickers;

public class KlasePerson {
//Atribute
	protected String name;
	protected int telefon_nr;
	protected boolean bezahlen;
	
	//construktor
	public KlasePerson(String new_name, int new_telefon_nr, boolean new_bezahlen)
	{
	name = new_name;
	telefon_nr = new_telefon_nr;
	bezahlen = new_bezahlen;
	}
	//getter
	public String getName () { return name;}
	public int getTelefon_nr() { return telefon_nr;}
	public boolean getBezahlen() {return bezahlen;}
	
	//setter
	public void setName (String name) {this.name = name;}
	public void setTelefon_nr (int telefon_nr) {this.telefon_nr = telefon_nr;}
	public void setBezahlen (boolean bezahlen) {this.bezahlen = bezahlen;}
}
